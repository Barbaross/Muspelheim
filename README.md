# Muspelheim

![Example Scrot](/screenshots/2022-09-19_12-10-40.png)

## Info
- OS: [Void Linux](https://voidlinux.org/)
- Window Manager: [herbstluftwm](https://github.com/herbstluftwm/herbstluftwm)
- Terminal: [rxvt-unicode](http://software.schmorp.de/pkg/rxvt-unicode.html)
- Shell: [zsh](https://www.zsh.org/)
- Browser: [qutebrowser](https://github.com/qutebrowser/qutebrowser)
- Bar: [polybar](https://github.com/polybar/polybar)
- Fonts: Viscera, [Siji](https://github.com/stark/siji), and [Unifont](http://unifoundry.com/unifont/index.html)
- Music: [ncmpcpp](https://github.com/ncmpcpp/ncmpcpp) with [mpd](https://github.com/MusicPlayerDaemon/mpd)
- File manager: [ranger](https://github.com/ranger/ranger)
- Editor: [neovim](https://github.com/neovim/neovim)
- Email: [neomutt](https://github.com/neomutt/neomutt)
- Caldav: [khal](https://github.com/pimutils/khal) and [todoman](https://github.com/pimutils/todoman) with [vdirsyncer](https://github.com/pimutils/vdirsyncer)
- Chat: [weechat](https://github.com/weechat/weechat)
- RSS feeds: [newsboat](https://github.com/newsboat/newsboat)
- App Launcher: [rofi](https://github.com/davatorium/rofi)
- Notifications: [tiramisu](https://github.com/Sweets/tiramisu)
- Lock screen: [xsecurelock](https://github.com/google/xsecurelock)
- Color scheme: [alduin](https://github.com/AlessandroYorba/Alduin)

## Credits
- [xer0](https://github.com/xero/dotfiles)
- [pyratebeard](https://gitlab.com/pyratebeard/dotfiles)
- [JLErvin](https://github.com/JLErvin/dotfiles)
- [6gk](https://github.com/6gk/polka)
