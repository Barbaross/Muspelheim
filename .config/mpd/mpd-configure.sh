#!/usr/bin/env bash

password=$(pass mpd/barbaross)

cat <<EOF >/tmp/mpd.conf
bind_to_address            "localhost"
port                       "6600"
playlist_directory         "~/.local/share/mpd/playlists"
log_file                   "~/.local/share/mpd/mpd.log"
pid_file                   "/tmp/mpd.pid"
state_file                 "~/.local/share/mpd/mpd.state"

volume_normalization       "yes"
auto_update                "yes"
restore_paused             "yes"

# WebDAV setup
music_directory     "http://barbaross:$password@172.16.0.1:8080/music/"

# Get db from main server
database {
    plugin  "proxy"
    host    "172.16.0.1"
    port    "6600"
}

audio_output {
  type "pulse"
  name "Pulseaudio Output"
  audio_format "44100:16:2"
}

#audio_output {
#    type    "fifo"
#    name    "visualizer"
#    path    "/tmp/mpd.fifo"
#}
EOF
