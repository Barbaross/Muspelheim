#!/bin/env bash

#--------------------------------#
# Cover script to run with ncmpc #
#--------------------------------#

header() {
    clear
    printf "%b" "\033[1m\033[31mArtwork\033[0m\n"
    pad=""
    for i in $(seq 1 "$(tput cols)"); do
      pad="─$pad"
    done
    printf "%b" "\033[1m\033[30m${pad}\033[0m"
}

header

playerctl -F -p mpd metadata mpris:artUrl |
  while read -r event; do
    header
    art=$(printf "%s\n" "$event" | sed 's|file://||')

    ## Attempt at calculating percentage offset needed to keep pic in the upper
    ## left position, without being under the title
    font_height=16
    cell_height=$(tput lines)
    pix_height=$((cell_height * font_height))
    y_offset=$(echo "100 * ((4 * $font_height) / $pix_height)" | bc -l | cut -d'.' -f1)

    ## Issue draw command depending on whether we're in a tmux session
    if [ -n "$TMUX" ]; then
      printf "\ePtmux;\e\e]20;%s;24x100+100+%s:op=keep-aspect\a\e\\" "$art" "$y_offset"
    else
      printf "\e]20;%s;24x100+100+%s:op=keep-aspect\a" "$art" "$y_offset"
    fi
  done
