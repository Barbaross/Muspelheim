#!/bin/env bash

# Cleanup when we're finished
trap "kill 0" SIGINT SIGTERM EXIT

# Suppress error messages
exec 2>/dev/null

bltth() {
  rfkill event |
    while read -r line; do
      status=$(echo "$line" | grep "type 2" | awk '{print $10}')

      [ -z $status ] && status=1
      if [ "$status" -eq 0 ]; then
        status="%{F#87afaf}%{T1}%{T-}%{F-}"
      else
        status=""
      fi
      echo "BLUE ${status}"

    done
}

vpninfo() {
  vpnicon() {
    cvpn=""
    vpn="$(ip addr | grep 'mullgard\|tun0' | awk '{print $2}' | grep -v -E '^[0-9]+' | tr -d ':')"
    for v in $vpn; do
      case "$v" in
      mullgard)
        cvpn+="%{F#af5f5f}%{T1}%{T-}%{F-}"
        ;;
      tun0)
        cvpn+="%{F#dfaf87}%{T1}%{T-}%{F-}"
        ;;
      esac
    done

    echo "VPN ${cvpn}"
  }
  vpnicon
  ip monitor netconf |
    while read -r line; do
      case "$line" in
      *mullgard* | *tun0* )
        vpnicon
        ;;
      esac
    done
}

caffeine() {
  caffeine_status="/tmp/caffeine_status"
  [ ! -f $caffeine_status ] && touch $caffeine_status
  tail -q -n 0 -F $caffeine_status |
    while read -r event; do
      # Caffeine status
      case "$event" in
      true)
        echo "CAFF %{F#af8787}%{T1}%{T-}%{F-}"
        ;;
      *)
        echo "CAFF "
        ;;
      esac
    done
}

notifs() {
  # if icon is visible, notifications are paused
  notif_pause="/tmp/notif_pause"
  # We actually rely on the error messages here
  tail -q -F "$notif_pause" 2>&1 |
    while read -r event; do
      case "$event" in
      *"cannot open"* | *"inaccessible"*)
        echo "NOTIF "
        ;;
      *)
        echo "NOTIF %{F#dfaf87}%{T1}%{T-}%{F-}"
        ;;
      esac
    done
}

{
  bltth &
  vpninfo &
  caffeine &
  notifs &
} |
  while read -r line; do
    case "$line" in
    BLUE*)
      bluetooth="${line:4}"
      ;;
    VPN*)
      vpn="${line:3}"
      ;;
    CAFF*)
      caffeine="${line:4}"
      ;;
    NOTIF*)
      notification="${line:5}"
      ;;
    esac

    if [ -n "$bluetooth" ] || [ -n "$vpn" ] || [ -n "$caffeine" ] || [ -n "$notification" ]; then
      echo "%{B#262626}$bluetooth$vpn$caffeine$notification %{B-}"
    else
      echo ""
    fi

  done
wait
