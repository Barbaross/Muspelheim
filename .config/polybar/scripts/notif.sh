#!/usr/bin/env bash

trap "kill 0" SIGINT SIGTERM EXIT

# Suppress error nessages such as the annoying "Terminated" message
exec 2>/dev/null

. "$HOME/.config/herbstluftwm/colorschemes/alduin.sh"
ORANGE="%{F$color11}"
CLR="%{B-}%{F-}"

# Notif logic
while :; do
  # Check periodically as the file is created by the notification service
  [ -f /tmp/notif_log ] && break
  sleep 1
done

old_notifs="/tmp/old_notifs"
[ ! -f $old_notifs ] && touch $old_notifs

tail -q -n 0 -F /tmp/notif_log $old_notifs |
  while read -r line; do
    # Attempt to avoid duplicate notifications if they are spammed
    active="/tmp/active_notif"
    prev="/tmp/prev_notif"
    echo "$line" >$active
    # Delete previous notif record if older than 10 seconds.
    find "$prev" -not -newermt '-10 seconds' -delete
    [ -f $prev ] || touch "$prev"
    cmp -s "$active" "$prev" && continue
    mv $active $prev

    # We block the foreground until rofi is dead, if its running
    pid=$(pgrep -x rofi)
    [ -n "$pid" ] && tail --pid="$pid" -f /dev/null

    # Check if we're fullscreen; wait if we are before continuing
    current_win_state=$(xprop -id "$(xdo id)" _NET_WM_STATE | cut -d' ' -f3)
    if [ "$current_win_state" = "_NET_WM_STATE_FULLSCREEN" ]; then
      # 'property' event monitors focus changes while
      # 'substructure' event monitors fullscreen state
      xev -root -event property -event substructure |
        while read -r; do
          break
        done
    fi

    # Block foreground until pause file gets deleted
    # if it was created by the toggle.sh script
    [ -f "/tmp/notif_pause" ] && inotifywait -q -q /tmp/notif_pause

    # Duplicate any '%' to process them as literal '%' in polybar
    line=${line//%/%%}

    # Extract the relvant info
    body="$(echo -e "$line" | cut -f1 -)"
    summary="$(echo -e "$line" | cut -f2 -)"
    timeout="$(echo -e "$line" | cut -f3 -)"

    # Text that comes before the notif summary
    pretext="${ORANGE} %{T5}$body:%{T-}${CLR} "

    # Determine actual timeout in secs
    if [ "$timeout" = "-1" ]; then
      time=10.0
    else
      time=$(echo "scale=1;$timeout/1000" | bc)
    fi

    # And finally, display the notification
    length=80
    wait_time=1 # Amount of time to display notif before starting scroll
    rem_time=$(echo "scale=1;$time - $wait_time" | bc -l)
    echo "$summary" | zscroll -l "$length" -t "$wait_time" -s "false" -b "$pretext" -a "..." &&
      echo "$summary" | zscroll -l "$length" -t "$rem_time" -d 0.2 -b "$pretext" -a "..."

    echo "${CLR}"
  done
