#!/bin/sh

# Wrapper for rss notifications
# First word should be 'Newsboat', as the notif's summary.
# The rest is the notification's body
summary=$(echo "$1" | cut -d' ' -f1)
body=$(echo "$1" | cut -d' ' -f2-)
notify-send "$summary" "$body"
