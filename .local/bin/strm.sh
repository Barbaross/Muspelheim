#!/bin/sh

# Wrapper script for qutebrowser

# Stream a link from remote server
pw=$(pass mpd/barbaross | tr -d '\n')
furl=$(echo "$1" | sed "s|http://|http://barbaross:${pw}@|")

mpv "$furl"
