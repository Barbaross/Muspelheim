#!/bin/bash

# Example notifier script -- lowers screen brightness, then waits to be killed
# and restores previous brightness on exit.

## CONFIGURATION ##############################################################

# Brightness will be lowered to this value.
min_brightness=10

# Time to sleep (in seconds) between increments
fade_step_time=0.0025

###############################################################################

get_brightness() {
  brightnessctl -m | cut -d',' -f4 | tr -d '%'
}

set_brightness() {
  brightnessctl -q set "$1%"
}

fade() {
  local level
  for level in $(eval echo {$(get_brightness)..$1}); do
    set_brightness "$level"
    sleep $fade_step_time
  done
}

# Commands to run before screen dim
old_brightness=$(get_brightness)
toggle.sh notifs pause
fade $min_brightness

# Sunglasses time
xinput test-xi2 --root |
  while read -r event; do
    case "$event" in
    *EVENT*)
      break
      ;;
    esac
  done

# Commands to run after resuming
pgrep -x xsecurelock && pkill -x -USR2 xsecurelock
set_brightness "$old_brightness"
toggle.sh notifs resume
