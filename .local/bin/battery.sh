#!/bin/sh

warning() {
  battery_level=$(cat /sys/class/power_supply/BAT0/capacity)
  battery_status=$(cat /sys/class/power_supply/BAT0/status)
  if [ "$battery_level" -le 5 ] && [ "$battery_status" != "Charging" ]; then
    notify-send -t 10000 -u critical "Battery" "WARNING!! SYSTEM IS SHUTTING DOWN IN 15 SECONDS!!"
    sleep 15
    sudo ZZZ
  elif [ "$battery_level" -le 10 ] && [ "$battery_status" != "Charging" ]; then
    notify-send -u critical -t 5000 "Battery" "Charge is running low! ${battery_level}% remaining!"
  elif [ "$battery_level" -ge 100 ]; then
    notify-send -t 10000 "Battery" "Charge is full. Disconnect the power."
  fi

  # Bluetooth mouse battery
  bt_bat_check=$(find /sys/class/power_supply/* | wc -l)
  if [ "$bt_bat_check" -eq 3 ]; then
    bt_bat_lvl=$(cat /sys/class/power_supply/hid-34:88:5d:b6:e9:13-battery/capacity)
    if [ "$bt_bat_lvl" -le 10 ]; then
      notify-send -u critical -t 5000 "Battery" "Bluetooth battery charge is running low! ${bt_bat_lvl}% remaining!"
    fi
  fi
}

powertime() {
  status=$(cat /sys/class/power_supply/BAT0/status)

  if [ "$status" = "Charging" ]; then
    time=$(acpi -b | cut -d' ' -f5)
    notify-send "Battery" "Roughly $time until fully charged"
  elif [ "$status" = "Not charging" ]; then
    notify-send "Battery" "AC mode activated"
  elif [ "$status" = "Full" ]; then
    notify-send "Battery" "Full"
  else
    time=$(acpi -b | cut -d' ' -f5)
    notify-send "Battery" "Roughly $time remaining"
  fi
}

case "$1" in
-t | --time)
  powertime
  ;;
*)
  warning
  ;;
esac
