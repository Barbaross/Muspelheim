#!/bin/sh

# The one toggle script to rule them all

bar() {
  map_state=$(xwininfo -id $(xdo id -N Polybar | head -1) | awk '/Map State/ {print $3}')

  if [ "$map_state" = "IsViewable" ]; then
    polybar-msg cmd hide
    toggle.sh notifs pause
  else
    polybar-msg cmd show
    toggle.sh notifs resume
  fi
}

notifs() {
  file="/tmp/notif_pause"

  pause() {
    echo "pause" >"$file"
  }

  resume() {
    rm -f "$file"
  }

  case "$1" in
  pause)
    pause
    ;;
  resume)
    resume
    ;;
  *)
    if [ -f "$file" ]; then
      resume
    else
      pause
    fi
    ;;
  esac
}

compositor() {
  if pidof picom >/dev/null; then
    sv down ~/.local/service/x11/picom
    notify-send "Compositor" "Killed"
  else
    sv up ~/.local/service/x11/picom
    notify-send "Compositor" "Started"
  fi
}

touchpad() {
  state=$(xinput list-props "Elan Touchpad" | grep "Device Enabled" | awk '{print $4}')

  if [ "$state" -eq 0 ]; then
    xinput set-prop "Elan Touchpad" "Device Enabled" 1 && notify-send "Libinput" "Touchpad enabled"
  elif [ "$state" -eq 1 ]; then
    xinput set-prop "Elan Touchpad" "Device Enabled" 0 && notify-send "Libinput" "Touchpad disabled"
  fi
}

case "$1" in
  bar)
    shift
    bar "$@"
    ;;
  notifs)
    shift
    notifs "$@"
    ;;
  compositor)
    shift
    compositor "$@"
    ;;
  touchpad)
    shift
    touchpad "$@"
    ;;
  *)
    echo "usage: $0 [bar|notifs|compositor|touchpad] <args>"
    ;;
esac
