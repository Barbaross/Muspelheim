#!/bin/sh

# Warp mouse to center of focused window
fwin=$(xdo id)
if [ -n "$fwin" ]; then
  winfo=$(xwininfo -id "$fwin")
  width=$(echo "$winfo" | awk 'NR==8 {print int($2/2)}')
  height=$(echo "$winfo" | awk 'NR==9 {print int($2/2)}')
  x=$(echo "$winfo" | awk 'NR==4 {print $4}')
  y=$(echo "$winfo" | awk 'NR==5 {print $4}')
  xdo pointer_motion -x $((width + x)) -y $((height + y))
fi
